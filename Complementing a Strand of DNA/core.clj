(defn reverse-complement
  [data]
  (loop [sym (first data)
         other (rest data)
         dna ""]
    (cond
      (= sym \T) (recur (first other)
                        (rest other)
                        (str \A dna))
      (= sym \G) (recur (first other)
                        (rest other)
                        (str \C dna))
      (= sym \C) (recur (first other)
                        (rest other)
                        (str \G dna))
      (= sym \A) (recur (first other)
                        (rest other)
                        (str \T dna))
      :else dna)))

(defn -main
  [& args]
  (println (reverse-complement (first args))))
