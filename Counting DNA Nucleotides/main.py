#!/usr/bin/env python3

# Author: Eric Nguyen
# Challenge: Counting DNA Nucleotides

def count_nucleotides(dna_data):
    """
    Return an array of the count of each DNA nucleotide, with the order being A, C, G, T.
    """
    return [dna_data.count("A"),
            dna_data.count("C"),
            dna_data.count("G"),
            dna_data.count("T")]

if __name__ == "__main__":
    import sys
    try:
        rosalind_txt = open(sys.argv[1], 'r')
        dna_data = rosalind_txt.read()
        print(count_nucleotides(dna_data))
    except IndexError:
        print("You need to specify the filename as an argument.")
    except FileNotFoundError:
        print("File not found.")
